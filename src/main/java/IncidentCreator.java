import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.json.JSONArray;
import org.json.JSONObject;

public class IncidentCreator {

    public static void main(String[] args) {
        String csrf_token;
        String org_id;
        String url = "https://app.resilientsystems.com/rest";
        JSONObject loginRequest = new JSONObject();
        loginRequest.put("email", "heman@ligadata.com").put("password", "LigaHemanData0");

        try {
            HttpResponse<JsonNode> response = Unirest.post(url + "/session")
                    .header("Content-Type", "application/json")
                    .body(new JsonNode(loginRequest.toString()))
                    .asJson();

            JSONObject responseBody = response.getBody().getObject();
            JSONArray orgs = (JSONArray) responseBody.get("orgs");

            csrf_token = (String) responseBody.get("csrf_token");
            org_id = String.valueOf(((JSONObject) orgs.get(0)).get("id"));

            JSONObject incidentObject = new JSONObject();
            incidentObject.put("name", "HemanApiTest3")
                    .put("description", "Test incident creation through API - Unirest")
                    .put("discovered_date", System.currentTimeMillis());

            HttpResponse<JsonNode> incidentsCreateResponse = Unirest.post(url + "/orgs/" + org_id + "/incidents")
                    .header("Content-Type", "application/json")
                    .header("X-sess-id", csrf_token)
                    .body(new JsonNode(incidentObject.toString()))
                    .asJson();

            JSONObject incidentsCreateResponseBody = incidentsCreateResponse.getBody().getObject();

            System.out.println(incidentsCreateResponseBody.toString());

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

        }
    }

}
